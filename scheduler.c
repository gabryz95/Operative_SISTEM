#include <stdlib.h>
#include <printf.h>
#include <sys/time.h>
#include "scheduler.h"
#include "bthread.h"
#include "bthread_private.h"

void *random_scheduling() {
    volatile __bthread_scheduler_private *my_scheduler = bthread_get_scheduler();
    volatile __bthread_private *my_thread;
    TQueue tQueue;

    do {
        double value = get_current_time_millis();
        unsigned long random_value = rand() % tqueue_size(my_scheduler->queue);
        tQueue = tqueue_at_offset(my_scheduler->queue, random_value);
        my_thread = tqueue_get_data(tQueue);
        if ((my_thread->state == __BTHREAD_SLEEPING) && (value > my_thread->wake_up_time))
            my_thread->state = __BTHREAD_READY;
    } while (my_thread->state != __BTHREAD_READY);

    my_scheduler->current_item = tQueue;
}

//chi ha la priorità maggiore sarà eseguito
void *priority_scheduling() {
    volatile __bthread_scheduler_private *my_scheduler = bthread_get_scheduler();
    unsigned long size = tqueue_size(my_scheduler->queue);
    volatile unsigned int priority = 0;
    TQueue tQueue = NULL, list = NULL;
    int cnt = 0;

    do {
        for (int i = 0; i < size; ++i) {
            volatile __bthread_private *thread = tqueue_get_data(tqueue_at_offset(my_scheduler->queue, i));
            double value = get_current_time_millis();
            bthread_state state = thread->state;
            if ((state == __BTHREAD_SLEEPING) && (value > thread->wake_up_time))
                thread->state = __BTHREAD_READY;
            if (state == __BTHREAD_READY && thread->priority != 0)
                tqueue_enqueue(&list, thread);
            else
                cnt++;
        }
        if (cnt == size) {
            random_scheduling();
            return 0;
        }
    } while (list == NULL);


    unsigned long sizeList = tqueue_size(list);

    do {
        for (int j = 0; j < sizeList; ++j) {
            volatile __bthread_private *thread = tqueue_get_data(tqueue_at_offset(list, j));
            if (thread->priority > priority) {
                priority = (volatile unsigned int) thread->priority;
                tQueue = thread;
            }
        }
    } while (tQueue == NULL);
    my_scheduler->current_item = tQueue;
}

void *round_robin_scheduler() {
    volatile __bthread_scheduler_private *my_scheduler = bthread_get_scheduler();
    volatile TQueue *tQueue = (TQueue *) tqueue_at_offset(my_scheduler->current_item, 0);
    volatile __bthread_private *my_thread = tqueue_get_data((TQueue) tQueue);

    double value = get_current_time_millis();
    if ((my_thread->state == __BTHREAD_SLEEPING) && (value > my_thread->wake_up_time))
        my_thread->state = __BTHREAD_READY;

    my_scheduler->current_item = tqueue_at_offset(my_scheduler->current_item, 1);
}

//TODO: sistemare il winner se m_ticket è uguale a zero
void *lottery_scheduler() {
    volatile __bthread_scheduler_private *my_scheduler = bthread_get_scheduler();
    unsigned long size = tqueue_size(my_scheduler->queue);
    int ticket_value = 1, m_ticket = 0;
    TQueue list = NULL, tQueue = NULL;

    do {
        for (int i = 0; i < size; ++i) {
            volatile __bthread_private *my_thread = tqueue_get_data(tqueue_at_offset(my_scheduler->queue, i));
            double value = get_current_time_millis();
            bthread_state state = my_thread->state;
            if ((state == __BTHREAD_SLEEPING) && (value > my_thread->wake_up_time))
                my_thread->state = __BTHREAD_READY;
            if (state == __BTHREAD_READY)
                tqueue_enqueue(&list, my_thread);
        }
    } while (list == NULL);

    unsigned long listSize = tqueue_size(list);
    int lottery[listSize], ticket[listSize][10000]; //TODO: sistemare grandezza in base alla rpiorità se c'è e non alla lunghezza della lista

    for (int j = 0; j < listSize; ++j) {
        volatile __bthread_private *thread = tqueue_get_data(tqueue_at_offset(list, j));
        if (thread->priority == 0)
            lottery[j] = (int) ((rand() % listSize - 1) + 1);
        else
            lottery[j] = thread->priority;
        for (int i = 0; i < lottery[j]; ++i) {
            ticket[j][i] = ticket_value++;
            m_ticket = ticket_value;
        }
    }

    do {
        int winner = (rand() % m_ticket - 1) + 1;
        for (int k = 0; k < listSize; ++k) {
            for (int i = 0; i < listSize; ++i) {
                if (ticket[k][i] == winner) {
                    tQueue = tqueue_at_offset(list, (unsigned long) k);
                    break;
                }
            }
        }
    } while (tQueue == NULL);
    my_scheduler->current_item = tQueue;
}


void task_set_priority(int priority) {
    volatile __bthread_scheduler_private *my_scheduler = bthread_get_scheduler();
    volatile __bthread_private *my_thread = tqueue_get_data(my_scheduler->current_item);
    my_thread->priority = priority;
}


